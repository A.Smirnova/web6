function counting() {
    var k = document.getElementById("number").value;
    if ( isNaN(k) || k < 0 ) alert("Введенное значение не является числом. Введите данные правильно.");
    else {
      var typeOf = document.getElementById("type").value;
      var itog;
      if (typeOf == 30){
        itog = 30*k;
        document.getElementById("cost").innerHTML = itog;
      }
      else if (typeOf == 20){
        var r = document.getElementsByName('radi');
        for(var i = 0; i < r.length; i++) {
          if(r[i].checked) var rad = r[i].value;
        }
        itog = 20*k*rad;
        document.getElementById("cost").innerHTML = itog;
      }
      else if (typeOf == 200){
        var r1 = document.getElementById("ch1");
        var r2 = document.getElementById("ch2");
        var dop = 0;
        if (r1.checked) dop -= -r1.value;
        if (r2.checked) dop -= -r2.value;
        itog = (200 + dop)*k;
        document.getElementById("cost").innerHTML = itog;
      }
    }
}

function changing() {
  var typeForElements = document.getElementById("type").value;
  var rad = document.getElementById("pen");
  var check = document.getElementById("album");
  if (typeForElements == 20) {
    rad.style.display = 'block';
    check.style.display = 'none';
  }
  else if (typeForElements == 200) {
    rad.style.display = 'none';
    check.style.display = 'block';
  }
  else if (typeForElements == 30){
    rad.style.display = 'none';
    check.style.display = 'none';
  }
  counting();
}

window.addEventListener('DOMContentLoaded', function (event) {
  console.log("DOM fully loaded and parsed");
  let kolvo = document.getElementById("number");
  let type = document.getElementById("type");
  let knopka = document.getElementById("pen");
  let box = document.getElementById("album");
  kolvo.addEventListener("input", counting);
  type.addEventListener("change", changing);
  knopka.addEventListener("change", counting);
  box.addEventListener("change", counting);
});

